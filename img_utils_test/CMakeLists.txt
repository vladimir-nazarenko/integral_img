cmake_minimum_required(VERSION 2.8)
project(integral_img_test)

set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fopenmp")

find_package(OpenCV ${TARGET_OpenCV_VERSION} EXACT REQUIRED)
find_package(GTest REQUIRED)

include_directories(..)

set(SOURCES main.cpp
        test_integral_img.cpp)

add_executable(integral_img_test ${HEADERS} ${SOURCES})
target_link_libraries(integral_img_test img_utils)
