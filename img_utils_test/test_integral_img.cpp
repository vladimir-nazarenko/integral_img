#include <catch2/catch.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <img_utils/integral_img.h>
#include <iostream>
#include <fstream>

namespace {
	bool mat_equal(const cv::Mat &a, const cv::Mat &b)
	{
		cv::Mat diff;
		cv::absdiff(a, b, diff);
		const double eps = 1e-16;
		return a.type() == b.type() && cv::countNonZero(diff > eps) == 0;
	}
}

TEST_CASE( "Implementation is correct on small matrices", "[integral_img]" )
{
	SECTION("Zero-sized 32f matrix")
	{
		cv::Mat zero_sized(0, 0, CV_64FC1);
		cv::Mat expected(0, 0, CV_64FC1);
		img_utils::calculate_integral_image(zero_sized);
		REQUIRE(mat_equal(zero_sized, expected));
	}

	SECTION("Zero-rows 32i matrix")
	{
		cv::Mat zero_sized(0, 15, CV_64FC1);
		img_utils::calculate_integral_image(zero_sized);
		REQUIRE(zero_sized.rows * zero_sized.cols * zero_sized.channels() == 0);
	}

	SECTION("1x1x1 uint8")
	{
		cv::Mat input(1, 1, CV_64FC1, 43);
		cv::Mat expected(1, 1, CV_64FC1, 43);
		img_utils::calculate_integral_image(input, 1);
		REQUIRE(mat_equal(input, expected));
	}

	SECTION("ones 20x30x3")
	{
		cv::Mat input(20, 30, CV_64FC3, cv::Scalar(1, 1, 1));
		cv::Mat expected;
		cv::integral(input, expected);
		expected = expected(cv::Rect(1, 1, expected.cols - 1, expected.rows - 1));
		img_utils::calculate_integral_image(input, 1);
		REQUIRE(mat_equal(input, expected));
	}

	SECTION("1234x5678x4")
	{
		cv::Mat input(1234, 5678, CV_64FC4, cv::Scalar(3, 4, 5, 6));
		cv::Mat expected;
		cv::integral(input, expected);
		expected = expected(cv::Rect(1, 1, expected.cols - 1, expected.rows - 1));
		img_utils::calculate_integral_image(input, 1);
		REQUIRE(mat_equal(input, expected));
	}

	SECTION("2x2x4, 5 threads")
	{
		cv::Mat input(2, 2, CV_64FC4, cv::Scalar(3, 4, 5, 6));
		cv::Mat expected;
		cv::integral(input, expected);
		expected = expected(cv::Rect(1, 1, expected.cols - 1, expected.rows - 1));
		img_utils::calculate_integral_image(input, 5);
		REQUIRE(mat_equal(input, expected));
	}

	SECTION("2x7x2, 3 threads")
	{
		cv::Mat input(2, 7, CV_64FC2);
		input.col(0) = 9;
		for (int i = 1; i < 7; ++i) {
			input.col(i) = 2 * input.col(i - 1);
		}
		cv::log(input, input);
		cv::Mat expected;
		cv::integral(input, expected);
		expected = expected(cv::Rect(1, 1, expected.cols - 1, expected.rows - 1));
		img_utils::calculate_integral_image(input, 3);
		REQUIRE(mat_equal(input, expected));
	}

	SECTION("eye 100x100, 3 threads")
	{
		cv::Mat input = cv::Mat::eye(100, 100, CV_64FC1);
		cv::Mat expected;
		cv::integral(input, expected);
		expected = expected(cv::Rect(1, 1, expected.cols - 1, expected.rows - 1));
		img_utils::calculate_integral_image(input, 3);
		REQUIRE(mat_equal(input, expected));
	}

	SECTION("throws when 0 threads")
	{
		cv::Mat input = cv::Mat::eye(100, 100, CV_64FC1);
		REQUIRE_THROWS(img_utils::calculate_integral_image(input, 0));
	}

	SECTION("throws when negative threads")
	{
		cv::Mat input = cv::Mat::eye(100, 100, CV_64FC1);
		REQUIRE_THROWS(img_utils::calculate_integral_image(input, -42));
	}

	SECTION("throws when img type is not CV_64F")
	{
		cv::Mat input = cv::Mat::eye(100, 100, CV_32FC2);
		REQUIRE_THROWS(img_utils::calculate_integral_image(input, 1));
	}

}