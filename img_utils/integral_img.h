#pragma once

#include <opencv2/core/core.hpp>

namespace img_utils {
/**
 * Calculates the integral image of \param img, (which is assumed to have CV_64F type)
 * and stores the result in the \param img without a buffer reallocation
 * using the \param num_threads threads, if possible.
 * If the number of elements in the matrix is zero, the function does nothing
 * @param img input/output buffer
 * @param num_threads desired thread number for computations
 * @throws std::invalid_argument if \param img has type other than CV_64F
 * @throws std::invalid_argument if \param num_threads is less or equal to zero
 */
	void calculate_integral_image(cv::Mat &img, const int num_threads=1);
}
