#include "integral_img.h"
#include <cassert>
#include <opencv2/core/core.hpp>
#include <iostream>
#include <functional>

namespace img_utils {
	namespace details {

		using DimGetter=std::function<cv::Mat(cv::Mat &, int)>;

		/*
		 * Implements a trivial parallelization scheme,
		 * see https://stackoverflow.com/a/35840595/2079157
		 */
		void integral_pass(cv::Mat &integral, int num_threads,
						   const int work_dim_size, DimGetter dim_getter,
						   const cv::Mat &ortho_zero_vector)
		{
			num_threads = std::min(num_threads, work_dim_size);
			const int chunk_size = work_dim_size / num_threads;
			std::vector<cv::Mat> global_corrections;
#pragma omp parallel num_threads(num_threads)
			{
#pragma omp for
				for (int offset = 0; offset < work_dim_size; offset += chunk_size) {
					for (int idx = offset + 1; idx < std::min(work_dim_size, offset + chunk_size); ++idx) {
						dim_getter(integral, idx) += dim_getter(integral, idx - 1);
					}
				}

#pragma omp barrier

#pragma omp single
				{
					cv::Mat global_correction = ortho_zero_vector;
					global_corrections.push_back(global_correction);
					for (int offset = chunk_size - 1; offset < work_dim_size; offset += chunk_size) {
						global_correction += dim_getter(integral, offset);
						global_corrections.push_back(std::move(global_correction));
						global_corrections.back().copyTo(global_correction);
					}
				}

#pragma omp barrier

#pragma omp for
				for (int offset = chunk_size; offset < work_dim_size; offset += chunk_size) {
					const auto &correction = global_corrections[offset / chunk_size];
					for (int idx = offset; idx < std::min(work_dim_size, offset + chunk_size); ++idx) {
						dim_getter(integral, idx) += correction;
					}
				}
			}
		}

		void horizontal_integral_pass(cv::Mat &integral, const int num_threads)
		{
			integral_pass(integral, num_threads, integral.cols, &cv::Mat::col,
						  cv::Mat::zeros(integral.rows, 1, integral.type()));
		}

		void vertical_integral_pass(cv::Mat &integral, const int num_threads)
		{
			integral_pass(integral, num_threads, integral.rows, &cv::Mat::row,
						  cv::Mat::zeros(1, integral.cols, integral.type()));
		}

//		void horizontal_integral_pass(cv::Mat &integral)
//		{
//			for (int ch = 0; ch < integral.channels(); ++ch) {
//				for (int row = 0; row < integral.rows; ++row) {
//					for (int col = 1; col < integral.cols; ++col) {
//						integral.ptr<double>(row, col)[ch] += integral.ptr<double>(row, col - 1)[ch];
//					}
//				}
//			}
//		}

//		void vertical_integral_pass(cv::Mat &integral)
//		{
//			for (int ch = 0; ch < integral.channels(); ++ch) {
//				for (int col = 0; col < integral.cols; ++col) {
//					for (int row = 1; row < integral.rows; ++row) {
//						integral.ptr<double>(row, col)[ch] += integral.ptr<double>(row - 1, col)[ch];
//					}
//				}
//			}
//		}

		void calculate_integral_image(cv::Mat &integral_img, const int num_threads)
		{
			horizontal_integral_pass(integral_img, num_threads);
			vertical_integral_pass(integral_img, num_threads);
		}
	}

	void calculate_integral_image(cv::Mat &img, const int num_threads)
	{
		if (img.empty() || img.rows == 0 || img.cols == 0) {
			return;
		}

		if (num_threads <= 0) {
			throw std::invalid_argument("Illegal threads number!");
		}

		if (img.depth() != CV_64F) {
			throw std::invalid_argument("Illegal matrix type!");
		}

		details::calculate_integral_image(img, num_threads);
	}
}
