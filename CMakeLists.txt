cmake_minimum_required(VERSION 2.8)
project(integral_img)

set(CMAKE_CXX_STANDARD 11)
set(TARGET_OpenCV_VERSION 3.2.0)

add_subdirectory(img_utils)
add_subdirectory(img_utils_test)
add_subdirectory(integral_img)
