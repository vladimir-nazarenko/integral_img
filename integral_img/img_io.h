#pragma once

namespace img_io
{
	/**
	 * Reads the image from the \param path to the \param img buffer
	 * @param path path ot image
	 * @param img output buffer
	 * @throws std::runtime_error if image cannot be read correctly
	 */
	void read_image(const std::string &path, cv::Mat &img);

	/**
	 * Writes the image from \param integral_img buffer to \param path. Works only for CV_64F
	 * @param path output path
	 * @param integral_img buffer to save
	 * @throws std::illegal_argument if the image type is other than CV_64F
	 * @throws std::runtime_error if the output file could not be created
	 */
	void write_integral_image(const std::string &path, const cv::Mat &integral_img);
}