#include <boost/program_options.hpp>
#include <iostream>
#include <omp.h>
#include <opencv2/core.hpp>
#include <img_utils/integral_img.h>
#include "img_io.h"

namespace po = boost::program_options;

bool parse_options(int argc, char **argv, std::vector<std::string> &img_paths, int &number_of_threads)
{
	try {
		po::options_description desc("Arguments");
		desc.add_options()
				("help", "print help message")
				(",t", po::value<int>(&number_of_threads)->default_value(0), "desired threads number")
				(",i", po::value<std::vector<std::string>>(&img_paths)->required(), "next input image");

		po::variables_map vm;
		po::store(po::parse_command_line(argc, argv, desc), vm);

		if (vm.count("help")) {
			std::cout << desc << "\n";
			return false;
		}

		po::notify(vm);
		if (number_of_threads < 0) {
			std::cout << "Illegal threads number: " << number_of_threads << std::endl;
			return false;
		}
	} catch (std::exception &e) {
		std::cerr << e.what() << std::endl;
		return false;
	} catch (...) {
		std::cerr << "Unexpected internal exception!" << std::endl;
		return false;
	}
	return true;
}

std::string add_extension(const std::string &path)
{
	return path + ".integral";
}

void process_img(const std::string &image_path, cv::Mat &img, const int num_threads)
{
	try {
		img_io::read_image(image_path, img);
		img.convertTo(img, CV_64F);
		img_utils::calculate_integral_image(img, num_threads);
		img_io::write_integral_image(add_extension(image_path), img);
	} catch (std::exception &e) {
#pragma omp critical
			std::cerr << e.what() << std::endl;
	}
}

void process_collection(const std::vector<std::string> &collection, bool parallel_img, const int num_threads)
{
	if (parallel_img) {
		cv::Mat img;

		for (const auto &image_path : collection) {
			process_img(image_path, img, num_threads);
		}
	} else {
#pragma omp parallel for num_threads(num_threads)
		for (int i = 0; i < collection.size(); ++i) {
			const auto &image_path = collection[i];
			cv::Mat img;
			process_img(image_path, img, 1);
		}
	}
}

int main(int argc, char **argv)
{
	std::vector<std::string> img_paths;
	int threads_number = 0;
	if (!parse_options(argc, argv, img_paths, threads_number)) {
		return 1;
	}

	if (threads_number == 0) {
		threads_number = omp_get_max_threads();
	}

	if (threads_number < img_paths.size()) {
		std::cout << "Using collection-level parallelism..." << std::endl;
		process_collection(img_paths, false, threads_number);
	} else {
		std::cout << "Using image-level parallelism..." << std::endl;
		process_collection(img_paths, true, threads_number);
	}

	return 0;
}