#include <opencv2/highgui.hpp>
#include <fstream>

namespace img_io
{
	void read_image(const std::string &path, cv::Mat &img)
	{
		img = cv::imread(path, cv::IMREAD_UNCHANGED);
		if (img.empty()) {
			throw std::runtime_error("Cannot read image " + path);
		}
	}

	void write_integral_image(const std::string &path, const cv::Mat &integral_img)
	{
		if (integral_img.depth() != CV_64F) {
			throw std::invalid_argument("Unsupported matrix type");
		}

		std::ofstream out_stream(path);

		if (out_stream.good()) {
			for (int ch = 0; ch < integral_img.channels(); ++ch) {
				for (int row = 0; row < integral_img.rows; ++row) {
					for (int col = 0; col < integral_img.cols; ++col) {
						out_stream << integral_img.ptr<double>(row, col)[ch] << ' ';
					}
					out_stream << "\n";
				}
				out_stream << std::endl << std::endl;
			}
		} else {
			throw std::runtime_error("Cannot save integral image " + path);
		}
	}
}